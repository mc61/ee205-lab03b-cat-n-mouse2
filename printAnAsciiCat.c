///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02b - Cat `n Mouse 2 - EE 205 - Spr 2022
/// @file    printAnAsciiCat.c
/// @version 1.0 - Initial version
///
/// @author  Caleb Mueller <mc61@hawaii.edu>
/// @date    27_JAN_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>

#define FILENAME ("ascii_cat.txt")

int main(void)
{
   int maxlength = 1024;
   FILE *fp = fopen(FILENAME, "r");
   char buffer[maxlength];

   while (fgets(buffer, maxlength, fp))
   {
      printf("%s", buffer);
   }
   fclose(fp);
}
