#!/bin/bash

declare -i DEFAULT_MAX_NUMBER=2048

if [[ -z $1 ]];
then
    THE_MAX_VALUE=$DEFAULT_MAX_NUMBER
else
    THE_MAX_VALUE=$1
fi

declare -i THE_NUMBER_IM_THINKING_OF=$(((RANDOM%THE_MAX_VALUE)+1))

declare -i EXIT_LOOP_CONDITION=0

while EXIT_LOOP_CONDITION=0
do
    echo "OK cat, I'm thinking of a number from 1 to $THE_MAX_VALUE"
    
    read A_GUESS

    if [ $A_GUESS -lt 1 ];
    then
        echo "You must enter a number that\'s >= 1"
        continue
    fi

    if [ $A_GUESS -lt $THE_NUMBER_IM_THINKING_OF ];
    then
        echo "No cat... the number I'm thinking of is larger than $A_GUESS"
        continue
    fi

    if [ $A_GUESS -gt $THE_NUMBER_IM_THINKING_OF ];
    then
        echo "No cat... the number I'm thinking of is smaller than $A_GUESS"
        continue
    fi

    if [ $A_GUESS == $THE_NUMBER_IM_THINKING_OF ];
    then
        gcc -o kitty.temp printAnAsciiCat.c && clear
        echo "You got me." && ./kitty.temp && rm -f ./kitty.temp
        break
    fi

done


